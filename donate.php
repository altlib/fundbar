<?php 
    get_header(); 
    $options = get_option( 'fundbar_settings' );
    $perks = json_decode($options["perks"], true);
?>
<script>
url = document.location.href.replace('?donate',  '');
perks = <?php echo $options["perks"]; ?>;
$(function(){
	$('#perkSelect').change();
	$("#notify_url").val(url + '?success');
	$("#return").val(url + '?thankyou');
	changePerk($("#s"));
});

function addFunder(){
    if(parseInt($("#amount").val()) > 0){
        if($("#name").val() && $("#email").val()){
            $.ajax({
                type: "POST",
                url: url + '?addfunder',
                data: $('#funder').serialize(),
                success: function(data){
                    if(data == "success")
                        $('#paypalForm').submit();
                }
            });
        }else{
            alert("Please enter your name and a valid email address");
        }
    }else{
        alert("Please enter an amount greater than 0");
    }
}

function changePerk(s){
    if($("option:selected",s).val() == "x"){
        $("#donation").val("0.00");
        $("#donation").attr('readonly', false);
    }else{
        $("#donation").val($("option:selected",s).val());
        $("#donation").attr('readonly', true);
    }

    $("#amount").val($("#donation").val());
    $("#item_name").val($("option:selected",s).text());
    $("#perk").val($("option:selected",s).html());
}
</script>
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div id="primary" class="content-area">
<a href="http://9gag.com/gag/a8jjOEO" target="_blank"><img class="aligncenter" src="http://altlib.org/wp-content/uploads/2015/07/a8jjOEO_700b.jpg" width=50%></a>
<form class="form-horizontal" id="funder">
<div class="form-group">
    <label class="control-label col-sm-2">Name</label>
    <div class="col-sm-6">
        <input class="form-control" type=text id="name" name="name">
    </div>
</div>
<div class="form-group">
    <label class="control-label col-sm-2">Email</label>
    <div class="col-sm-6">
        <input class="form-control" type=text id="email" name="email">
    </div>
</div>
<div class="form-group">
        <label class="control-label col-sm-2">Perk</label>
        <div class="col-sm-6">
<?php
	foreach($perks as $perk){
    	if(is_numeric($perk['amount']))
    	    $perk['amount'] = $perk['amount'] . ".00";
        $perkList[$perk['amount']] = $perk['name'];
	}
	echo '<select id="s" onchange=\'changePerk(this)\'>';
	foreach($perkList as $val=>$perk){
    	echo "<option value='$val'>$perk</option>";
    	
	}
	echo "</select>";
?>
    <input id="donation" name="donation" onchange="$('#amount').val($(this).val());" value="0.00">

        </div>
</div>
    <input type="hidden" id="perk" name="perk">

<div class="form-group">
        <label class="control-label col-sm-2">&nbsp;</label>
                <div class="col-sm-6">

<a href="javascript:void(0)"  onclick="addFunder()"><img src="https://www.paypalobjects.com/en_US/i/btn/btn_donate_SM.gif" style="float:left"></a>
<img src="https://www.paypalobjects.com/webstatic/mktg/logo/AM_mc_vs_dc_ae.jpg" border="0" alt="PayPal Acceptance Mark" height=55 style="vertical-align:middle;padding-left:10px">

                </div>
</div>

</form>





</div>


</div>

<form id="paypalForm" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top" style="display:none">
<input type="hidden" name="cmd" value="_donations">
<input type="hidden" name="business" value="PHW6R99QAAUYN">
<input type="hidden" name="lc" value="US">
<input type="hidden" id="item_name" name="item_name" value="Bellingham Alternative Library">
<input type="hidden" name="no_note" value="1">
<input type="hidden" id="amount" name="amount" value="0.00">
<input type="hidden" name="no_shipping" value="1">
<input type="hidden" name="rm" value="1">
<input type="hidden" id="return" name="return" value="">
<input type="hidden" name="cancel_return" value="http://altlib.org/">
<input type="hidden" name="currency_code" value="USD">
<input type="hidden" name="bn" value="PP-DonationsBF:btn_donate_SM.gif:NonHosted">
<input type="hidden" id="notify_url" name="notify_url" value="">
<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donate_SM.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
</form>
        </div></div></div>
<?php
get_footer();
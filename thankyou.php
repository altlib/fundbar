<?php get_header(); ?>
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div id="primary" class="content-area">
              Thanks for your donation!<br><br>
              <a class="btn btn-success" href="http://altlib.org/fundbar/alt-lib-fundraiser/">Back</a>
          </div>
        </div>
      </div>
    </div>
<?php get_footer(); ?>
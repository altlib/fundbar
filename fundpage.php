<?php if(get_post_type( get_the_ID() ) == "fundbar"): ?>
<?php get_header(); ?>
<style>
#fundbar hr {
    border-top:1px solid #aaa;
    margin-bottom:0;
}
#fundbar h3 {
    margin-top:10px;
}
.donateButton{
	background:#0c0;
	color:#fff;
	border-radius:5px;
	border:0;
	padding:10px;
	font-size:16px;
	font-weight: 700;
}
.donateButton:hover{
	background:#ebeb11;
	color:#111;
}
.progress-bar {
	background-color: #1a1a1a;
	height: 25px;
	padding: 5px;
	width: 100%;
	margin: 10px 0;
	-moz-border-radius: 5px;
	-webkit-border-radius: 5px;
	border-radius: 5px;
	-moz-box-shadow: 0 1px 5px #000 inset, 0 1px 0 #444;
	-webkit-box-shadow: 0 1px 5px #000 inset, 0 1px 0 #444;
	box-shadow: 0 1px 5px #000 inset, 0 1px 0 #444;
}

.progress-bar span {
	display: inline-block;
	height: 100%;
	-moz-border-radius: 3px;
	-webkit-border-radius: 3px;
	border-radius: 3px;
	-moz-box-shadow: 0 1px 0 rgba(255, 255, 255, .5) inset;
	-webkit-box-shadow: 0 1px 0 rgba(255, 255, 255, .5) inset;
	box-shadow: 0 1px 0 rgba(255, 255, 255, .5) inset;
        -webkit-transition: width .4s ease-in-out;
        -moz-transition: width .4s ease-in-out;
        -ms-transition: width .4s ease-in-out;
        -o-transition: width .4s ease-in-out;
        transition: width .4s ease-in-out;
}
.green span {
      float:left;
	  background-color: #a5df41;
	  background-image: -webkit-gradient(linear, left top, left bottom, from(#a5df41), to(#4ca916));
	  background-image: -webkit-linear-gradient(top, #a5df41, #4ca916);
	  background-image: -moz-linear-gradient(top, #a5df41, #4ca916);
	  background-image: -ms-linear-gradient(top, #a5df41, #4ca916);
	  background-image: -o-linear-gradient(top, #a5df41, #4ca916);
	  background-image: linear-gradient(top, #a5df41, #4ca916);
}
.shine span {
	position: relative;
}

.shine span::after {
	content: '';
	opacity: 0;
	position: absolute;
	top: 0;
	right: 0;
	bottom: 0;
	left: 0;
	background: #fff;
	-moz-border-radius: 3px;
	-webkit-border-radius: 3px;
	border-radius: 3px;

	-webkit-animation: animate-shine 2s ease-out infinite;
	-moz-animation: animate-shine 2s ease-out infinite;
}

@-webkit-keyframes animate-shine {
	0% {opacity: 0; width: 0;}
	50% {opacity: .5;}
	100% {opacity: 0; width: 95%;}
}


@-moz-keyframes animate-shine {
	0% {opacity: 0; width: 0;}
	50% {opacity: .5;}
	100% {opacity: 0; width: 95%;}
}
</style>
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div id="primary" class="content-area">
    <div class="col-md-8">
        <?php 
            if ( have_posts() ) : the_post();

            the_content();
            
            endif;
         ?>
        
    </div>
    <div class="col-md-4">
<?php
$options = get_option( 'fundbar_settings' );
$goal = $options["goal"];
$perks = json_decode($options["perks"], true);
$claimed = array();
if($perks){
	foreach($perks as $perk){
        if(is_numeric($perk['amount']))
		    $claims =  $wpdb->get_var("SELECT count(id) FROM wp_fundbar WHERE perk = '" . $perk['name'] . "' AND status='confirmed'");
		else
		    $claims =  $wpdb->get_var("SELECT sum(amount) FROM wp_fundbar WHERE perk = '" . $perk['name'] . "' AND status='confirmed'");
        if(!$claims)
            $claims = 0;
		$claimed[$perk['name']] = $claims;
	}
}
		$donations = $wpdb->get_var("SELECT SUM(amount) FROM wp_fundbar WHERE status = 'confirmed'");
		if($goal > 0)
		    $progress = intval($donations / $goal  * 100);
		else
		    $progress = 0;

		echo "<h1>$" . $donations . " / $" . $goal;
echo '<div class="progress-bar green shine">
    <span style="width:'. $progress . '%"></span>
</div>';

		echo "</h1>
		<button class='donateButton' onclick='document.location=document.location.href+\"?donate\"'>Donate Now</button>
<div id='fundbar' style='margin-top:10px;border-radius:10px;background:#AEB;padding:10px'><b>Perks</b><br><br>";

		foreach($perks as $perk){
			echo "<h3>" . $perk['name'] . "</h3>";
			echo "<p>" . $perk['description'] . "</p><br>";
			if(is_numeric($perk['amount'])){
			    echo "<p><b>$" . $perk['amount'] . ".00</b><br>";
                echo $claimed[ $perk['name']] .  " / " . $perk['limit'] ." claimed</p>";
			}else{
			    echo "<p><b>Any Amount</b><br>";
                echo "$" . $claimed[ $perk['name']] . " donated so far</p>";
			}
			echo "<hr><br style='clear:both'>";
		}

		echo "<br style='clear:both'>";

?>
		<button class='donateButton' onclick='document.location=document.location.href+"?donate"'>Donate Now</button>
		</div>
    </div>
    </div><!--/#primary-->
</div><!--/.col-lg-12-->
</div><!--/.row-->
</div><!--/.container.-->
<?php
get_footer(); ?>
<?php else: ?>
<?php endif; ?>
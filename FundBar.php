<?php
/**
 * Plugin Name:	Fund Bar
 * Plugin URI:	http://altlib.org
 * Description:	A custom fundraising widget for Wordpress
 * Version: 	1.90.0
 * Author:		David Zhang, Matt Gilmore
 * Author URI:	http://altlib.org
 * License:		GPL v2 or later
 * License URI:	https://www.gnu.org/licenses/gpl-2.0.html
 */
class FundBar
{

    static private $pluginFile = __FILE__;
    private $version;
    private $admin_page;
    private $short_code_class;
    public $templates;
    static function _getVersion()
    {
        $version = '';
        $fp = fopen(self::$pluginFile, 'r');
        if ($fp)
        {
            while ($line = fgets($fp))
            {
                if (preg_match("/^\s*\*\s*$/", $line) > 0)
                    {break;}
                if (preg_match('/^\s*\*\s*Version:\s*(.*)$/', $line, $matches) > 0)
                {
                    $version = $matches[1];
                    break;
                }
            }
            fclose($fp);
        }
        return $version;
    }

    /* Constructor: register our activation and deactivation hooks and then
     * add in our actions.
     */
    function __construct()
    {
        $this->version = self::_getVersion();
        // Add the installation and uninstallation hooks
        register_activation_hook(__FILE__, array($this, 'install'));
        register_deactivation_hook(__FILE__, array($this, 'deinstall'));
        add_action('init', array($this, 'init_action'));
        add_filter('single_template', array($this, 'fundbar'), 2);
        add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
        add_action( 'admin_init', array( $this, 'page_init' ) );
        add_filter( 'get_pages', array($this, 'wpa18013_add_pages_to_dropdown'), 10, 2 );
        add_action( 'pre_get_posts', array($this, 'enable_front_page_stacks'));

    }

    function install()
    {
        global $wpdb;
        $sql = "CREATE TABLE wp_fundbar (
        id int(11) NOT NULL AUTO_INCREMENT unique,
        name text NOT NULL,
        email text NOT NULL,
        amount text NOT NULL,
        perk text NOT NULL,
        status text NOT NULL,
        date datetime NOT NULL,
        PRIMARY KEY  (id)
        );";
    
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);
    }
    
    function deinstall()
    {
    
    }
    
    function init_action()
    {
            $args = array(
            'public' => true,
            'query_var' => 'fundbar',
            'rewrite' => array(
                'slug' => 'fundbar',
                'with_front' => false,
            ),
            'supports' => array('title', 'editor', 'thumbnail', 'comments'),
            'labels' => array(
                'name' => 'Fund Bars',
                'singular_name' => 'Fund Bar',
                'add_new' => 'Add New Fund Bar',
                'add_new_item' => 'Add New Fund Bar',
                'edit_item' => 'Edit Fund Bar',
                'new_item' => 'New Fund Bar',
                'view_item' => 'View Fund Bar',
                'search_items' => 'Search Fund Bar',
                'not_found' => 'No Fund Bars Found',
                'not_found_in_trash' => 'No Fund Bars Found In Trash'
            ),
        );
        register_post_type('fundbar', $args );

    }

    public function enable_front_page_stacks( $query ){
        if(isset($query->query_vars['post_type']) &&  '' == $query->query_vars['post_type'] && 0 != $query->query_vars['page_id'])
            $query->query_vars['post_type'] = array( 'page', 'fundbar' );
    }

    public function wpa18013_add_pages_to_dropdown( $pages, $r ){

            $args = array(
                'post_type' => 'fundbar'
            );
            $stacks = get_posts($args);
            $pages = array_merge($pages, $stacks);
    
        return $pages;
    }

    /**
     * Add options page
     */
    public function add_plugin_page()
    {
        add_submenu_page(
            'edit.php?post_type=fundbar',
            'Fund Bar Settings',
            'Fund Bar Settings',
            'manage_options',
            'my-setting-admin',
            array( $this, 'create_admin_page' )
        );
    }

    /**
     * Options page callback
     */
    public function create_admin_page()
    {
        // Set class property
        $this->options = get_option( 'fundbar_settings' );
        ?>
        <div class="wrap">
            <form method="post" action="options.php">
            <?php
                // This prints out all hidden setting fields
                settings_fields( 'my_option_group' );   
                do_settings_sections( 'my-setting-admin' );
                submit_button(); 
            ?>
            </form>
        </div>
        <?php
    }

    /**
     * Register and add settings
     */
    public function page_init()
    {        
        register_setting(
            'my_option_group', // Option group
            'fundbar_settings', // Option name
            array( $this, 'sanitize' ) // Sanitize
        );

        add_settings_section(
            'setting_section_id', // ID
            'Fund Bar Settings', // Title
            array( $this, 'print_section_info' ), // Callback
            'my-setting-admin' // Page
        );  

        add_settings_field(
            'goal', // ID
            'Goal', // Title 
            array( $this, 'goal_callback' ), // Callback
            'my-setting-admin', // Page
            'setting_section_id' // Section           
        );      

        add_settings_field(
            'perks', 
            'Perks', 
            array( $this, 'perks_callback' ), 
            'my-setting-admin', 
            'setting_section_id'
        );      
    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize( $input )
    {
        $new_input = array();
        if( isset( $input['goal'] ) )
            $new_input['goal'] = absint( $input['goal'] );

        if( isset( $input['perks'] ) )
            $new_input['perks'] = sanitize_text_field( $input['perks'] );

        return $new_input;
    }

    /** 
     * Print the Section text
     */
    public function print_section_info()
    {
        printf('Enter your settings below:');
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function goal_callback()
    {
        printf(
            '<input type="text" id="goal" name="fundbar_settings[goal]" value="%s" />',
            isset( $this->options['goal'] ) ? esc_attr( $this->options['goal']) : ''
        );
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function perks_callback()
    {
        printf(
            '
<script>
count = 0;

function deleteRow(id){
	$("#"+id).remove();
	updatePerks();
}

function addRow(perk){
	template = $("#template").clone();

	template.attr("id",count);
	template.css("margin","0 0 10px 0");

	for(var k in perk){
		$("." + k, template).attr("name", "perks[" + count + "][" + k + "]");
		$("." + k, template).attr("onchange", "updatePerks()");
		$("." + k, template).val(perk[k]);
		console.log("perks[" + count + "][" + k + "]");
	}
	$("button", template).attr("onclick", "deleteRow(" + count + "); return false;");


	$("#dataform").append(template);
	$("#" + count).show();
	count++;
}

function updatePerks() {
    var perks = [];
    for(i = 0; i < count; i++){
        var perk = {
            name: $("input[name=\'perks[" + i + "][name]\']").val(),
            amount: $("input[name=\'perks[" + i + "][amount]\']").val(),
            description: $("textarea[name=\'perks[" + i + "][description]\']").val(),
            limit: $("input[name=\'perks[" + i + "][limit]\']").val()
        }
        perks.push(perk);
    }
    $("#perks").val(JSON.stringify(perks));
}

$(function() {
    perks = JSON.parse($("#perks").val());
    for (var k in perks){
	    addRow(perks[k]);
    }
});
</script>

        <textarea id="perks" name="fundbar_settings[perks]">%s</textarea>
        <div id="dataform"></div>
        <button onclick=\'addRow({name:"",amount:"",description:"",limit:""}); return false\'>Add</button>
			
    <div id="template" style="display:none">
		<div class="row">
			<label>Name</label>
			<input class="name" type=text>
		</div>
		<div class="row">
			<label>Amount</label>
			<input class="amount" type=text>
		</div>
		<div class="row">
			<label>Description</label>
			<textarea class="description"></textarea>
		</div>
		<div class="row">
			<label>Limit</label>
			<input class="limit" type=text>
		</div>
		<button style="width:60px;">Delete</button>
	</div>',
            isset( $this->options['perks'] ) ? esc_attr( $this->options['perks']) : ''
        );
    }

    function fundbar()
    {
        global $post;
	$single_template = '';
        if ($post->post_type == 'fundbar')
        {
            if (isset($_GET['donate'])) {
                $single_template = dirname( __FILE__ )  . '/donate.php';
            } else if (isset($_GET['addfunder'])) {
                $this->addfunder();
                echo "success";
                die();
            } else if (isset($_GET['success'])){
                $this->success();
                die();
            }else if (isset($_GET['thankyou'])) {
                $single_template = dirname( __FILE__ )  . '/thankyou.php';
            }else{
                $single_template = dirname( __FILE__ )  . '/fundpage.php';
            }
        }
        return $single_template;
    }

	private function success(){
        global $wpdb;
		if(isset($_POST) && isset($_POST['payer_email'])){
			$data = array('status'=>"confirmed");
			$id = $wpdb->get_var( "SELECT id FROM wp_fundbar WHERE email = '" . $_POST['payer_email']. "' ORDER BY id DESC" );
			if($id){
                $wpdb->update( 
                	'wp_fundbar', 
                	$data, 
                	array( 'id' => $id), 
                	array( 
                		'%s'
                	), 
                	array( '%d' ) 
                );
            }else{
            $wpdb->insert( 
            	'wp_fundbar', 
            	array( 
                    'name'=>"NOT FOUND",
                    'email'=>$_POST['payer_email'],
                    'amount'=>$_POST['payment_gross'],
                    'perk'=>"UNKNOWN",
                    'status'=>"confirmed",
                    'date'=> current_time('mysql', 1),
            	) 
            );
            }
		}
	}

    private function addfunder(){
        global $wpdb;
        if(isset($_POST)){
            $funder = $_POST;
            $wpdb->insert( 
            	'wp_fundbar', 
            	array( 
                    'name'=>$funder['name'],
                    'email'=>$funder['email'],
                    'amount'=>$funder['donation'],
                    'perk'=>$funder['perk'],
                    'status'=>"pending",
                    'date'=> current_time('mysql', 1),
            	) 
            );
        }
    }

}

$FundBar = new FundBar();
